\begin{acknowledgements}

It doesn't really matter in which order the acknowledgements and abstract come. In some ways it's nice to put the acknowledgements first then everything that follows is about the project you have done.

You should generally thank your supervisor(s) in your acknowledgements --- not doing so makes quite a statement. If anyone else has helped with the project then thank them too.

If you have received funding from anywhere, it is standard to thank your funding body.

It's then up to you who else to thank. You can be brief and stop there, or you can thank family, friends, the college cat \ldots

\end{acknowledgements}




# -*- coding: utf-8 -*-
"""
Backend algorithms for the infinite-dimensional string method

@author: Ioannis Papadopoulos
"""

from dolfin import *  # FEniCS
import numpy as np
import scipy as sc
import scipy.linalg
import matplotlib.pyplot as plt
from petsc4py import PETSc # PETSc

# Required to perform LDL^T decomposition in stability analysis
PETScOptions.set("mat_mumps_icntl_24", 1)
PETScOptions.set("mat_mumps_icntl_13", 1)

import sys, os
import matplotlib
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   :30}
matplotlib.rc('font', **font)

# Used to toggle output on and off
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__
################################# Define potential, inner product and angle

def rhs(Energy,u,v,coeff,mesh): # The negative of the Frechet derivative of the Energy functional
    # Can use manual differentiation for more accurate results e.g.
    #return -inner(grad(u), grad(v))*coeff[0]*dx - 1./coeff[0]*(u**3 - u)*v*dx - 0.1*coeff[3]*u.dx(0)
    return -derivative(Energy(u,coeff),u,v)# + 1e-4*u.dx(0)*v*dx

def innerh1(u,v):
    # The corresponding inner product for the chosen function space
    # Currently using H1 inner product
    return inner(u,v)*dx + inner(grad(u),grad(v))*dx

def angleh1(u,w): # Define the angle between two functions in corresponding function space
    costheta = assemble(innerh1(u,w))/(sqrt(assemble(innerh1(u,u))) * sqrt(assemble(innerh1(w,w))))
    return np.arccos(costheta)

def residue(u,w):
    return abs(assemble(innerh1(u,w))) -  sqrt(assemble(innerh1(u,u)))*sqrt(assemble(innerh1(w,w)))


################################# ODE Time Solvers

def TimeSolver(Energy,coeff,mesh,Type,phi,phi_prevs,v,dt,bcs):

    if Type == "CN": # Crank-Nicolson
        RHS = rhs(Energy,phi,v,coeff,mesh)
        RHS = replace(RHS, {phi: 0.5*phi + 0.5*phi_prevs[0]})
        F = innerh1(phi,v) - innerh1(phi_prevs[0],v) - dt*RHS
    elif Type == "bdf2": # Backward Difference Formula order 2
        F = innerh1(phi,v) - 4.0/3.0*innerh1(phi_prevs[0],v) + 1.0/3.0*innerh1(phi_prevs[1],v)\
                - 2.0/3.0*dt*rhs(Energy,phi,v,coeff,mesh)
    elif Type == "bdf3": # Backward Difference Formula order 3
        F = innerh1(phi,v) - 18.0/11.0*innerh1(phi_prevs[0],v) +9.0/11.0*innerh1(phi_prevs[1],v)\
                - 2.0/11.0*innerh1(phi_prevs[2],v) - 6.0/11.0*dt*rhs(Energy,phi,v,coeff,mesh)
    elif Type == "bdf4": # Backward Difference Formula order 4
        F= innerh1(phi,v) - 48.0/25.0*innerh1(phi_prevs[0],v) +36.0/25.0*innerh1(phi_prevs[1],v)\
                - 16.0/25.0*innerh1(phi_prevs[2],v) + 3.0/25.0*innerh1(phi_prevs[3],v)\
                        - 12.0/25.0*dt*rhs(Energy,phi,v,coeff,mesh)
    solve(F == 0, phi, bcs,solver_parameters={"newton_solver": {"maximum_iterations": 1000}})

    return phi

################################# Find Steady States and minima

# Script to calculate stability of steady state
# Modified code written by P. E Farrell, 2017, https://tinyurl.com/yagxf3jx, Date visited: 2.8.17
def compute_stability(Energy,rhs,u,bcs,coeff,mesh):
        V = u.function_space()
        trial = TrialFunction(V)
        v  = TestFunction(V)
        F = -rhs(Energy,u,v,coeff,mesh)
        comm = V.mesh().mpi_comm()
        J = derivative(F, u, trial)

        # Find dimension of problem
        dimension = V.dim()/len(mesh.coordinates())
        filler = 1.0 if dimension ==1 else [1.0]*dimension
        # a dummy linear form, needed to construct the SystemAssembler
        b = inner(Constant(filler), v)*dx
        # Build the LHS matrix
        A = PETScMatrix(comm)
        asm = SystemAssembler(J, b, bcs)
        asm.assemble(A)
        pc = PETSc.PC().create(comm)
        pc.setOperators(A.mat())
        pc.setType("cholesky")
        pc.setFactorSolverPackage("mumps")
        pc.setUp()
        F = pc.getFactorMatrix()
        (neg, zero, pos) = F.getInertia()
        expected_dim = 0
        # Nocedal & Wright, theorem 16.3
        if neg == expected_dim:
            is_stable = True
        else:
            is_stable = False
        d = is_stable
        return d

# Script for finding all steady states of energy functional through brute search
def findsteadystates(Energy,rhs,coeff,mesh,u,v,V,bcs,transitionplots=False):
    cache = {}
    count = 0
    for i in np.arange(-3, 3, 0.1): # Run through initial guesses -3 to 3
    #for i in [0.05]: # Run through initial guesses -3 to 3
        # Find dimension of system
        dimension = V.dim()/len(mesh.coordinates())
        eyes =  i if dimension ==1 else [i]*dimension
        u.interpolate(Constant(eyes))
        # Solve Frechet derivative = 0 for steady states

        solve(rhs(Energy,u,v,coeff,mesh) == 0, u, bcs, \
            solver_parameters={"newton_solver": {"maximum_iterations": 6000}})

        different = 0
        # Only keep solutions that have not already been discovered
        for iter in xrange(len(cache)):
            diff = errornorm(cache[iter],u,'H1')
            if diff <1e-3:
            # If H1 norm difference is less than 1e-3, then solution is deemed 'already discovered'
            # and script moves onto to next initial guess
                break
            else:
                different +=1
        # Assign new solution to a list
        if len(cache) == different or len(cache)==0:
            cache[count] = Function(V)
            cache[count].assign(u)
            count +=1

    print "%s steady states found" %len(cache.values())

    # Seperate solutions into minima and saddle/maxima
    stable = {}
    cache_min = {}
    cache_saddle= {}
    min_iter = 0; saddle_iter = 0
    for i in xrange(len(cache.values())):
        # Find the stability
        stable[i] = compute_stability(Energy,rhs,cache.values()[i],bcs,coeff,mesh)
        print "Fixed Point %s - Minimum: %s" % (i,stable[i])
        if stable[i] == True:
            # If minimum store in cache_min
            cache_min[min_iter] = Function(V)
            cache_min[min_iter].assign(cache.values()[i])
            min_iter +=1
        else:
            # If saddle point/maximum store in cache_saddle
            cache_saddle[saddle_iter] = Function(V)
            cache_saddle[saddle_iter].assign(cache.values()[i])
            saddle_iter +=1


    # If transitionplots option is True, then plot and save graphs of the steady states (in 1D)
    if transitionplots == True:
        for i2 in xrange(len(cache_min.values())):
                        xx = np.linspace(0,1,1000)
                        f = np.zeros((np.size(xx),dimension))
                        for i in np.arange(0, xx.size):
                            f[i,:] = cache_min[i2](xx[i])
                        for i in xrange(dimension):
                            plt.plot(xx,f[:,i],linewidth=3,label='Dimension %s' %(i+1))
                        plt.ylim([-3.5,3.5])
                        plt.xlabel('x')
                        plt.ylabel('y')
                        #plt.legend(loc='upper left',fontsize=15)
                        plt.savefig('Steady_States_Min_%s.png' %(i2), bbox_inches='tight')
                        plt.close("all")
        for i2 in xrange(len(cache_saddle.values())):
                        xx = np.linspace(0,1,1000)
                        f = np.zeros((np.size(xx),dimension))
                        for i in np.arange(0, xx.size):
                            f[i,:] = cache_saddle[i2](xx[i])
                        for i in xrange(dimension):
                            plt.plot(xx,f[:,i],linewidth=3,label='Dimension %s' %(i+1))
                        plt.ylim([-3.5,3.5])
                        plt.xlabel('x')
                        plt.ylabel('y')
                        #plt.legend(loc='upper left',fontsize=15)
                        plt.savefig('Steady_States_Saddle_%s.png' %(i2), bbox_inches='tight')
                        plt.close("all")
    plt.close("all")
    # Return the minima and saldde points/maxima
    return [cache_min, cache_saddle]

#################################  Splines
# linspline interpolates linearly from point to point
def linspline(x,xold,yold):
    i = 1
    while not (xold[i-1] <= x <= xold[i]): # Find which two images we wish to interpolate between
        i +=1
    # return the linear interpolation between the two images
    return Constant((xold[i] - x) / (xold[i] - xold[i-1])) * yold[i-1] + Constant((x - xold[i-1])\
                / (xold[i] - xold[i-1])) * yold[i]

# cubspline uses a cubic spline to interpolate between two images
def cubspline(x,xold,yold,B):
    i = 1
    while not (xold[i-1] <= x <= xold[i]):  # Find which two images we wish to interpolate between
        i +=1

    # B is a precalculated matrix containing the correct coefficients for the spline
    # Here we calculate the correct parameters
    D0 = B[i-1,0] * 3.0 * (yold[1] - yold[0]) + B[i-1,-1] * 3.0 * (yold[len(xold)-1]\
        - yold[len(xold)-2])
    D1 = B[i,0] * 3.0 * (yold[1] - yold[0]) + B[i,-1] * 3.0 * (yold[len(xold)-1] \
        - yold[len(xold)-2])
    for j in xrange(1,len(xold)-2):
        D0 = D0 + B[i-1,j] * 3.0 * (yold[j+1] - yold[j-1])
        D1 = D1 + B[i,j] * 3.0 * (yold[j+1] - yold[j-1])

    a = yold[i-1]
    b = D0
    c = 3.0 * (yold[i] - yold[i-1]) -2*D0 - D1
    d = 2.0 * (yold[i-1] - yold[i]) + D0 + D1

    t = (x - xold[i-1])/(xold[i] - xold[i-1])
    # Return cubic spline interpolate
    return a + b*t + c*t**2 + d*t**3

################################# Main script to find minimum energy path

def MEP(Energy,mesh,element,V,bcs,coeff,n = 20,cache_min=None,cache_saddle=None\
    ,para = 'cubic',initial = 'saddle',start = None,order = None,end  = None,pvdfile = False\
    ,pvdskip = 5,intermediategraphs = False, transitionplots = False,interskip = 10\
    ,dtset = 0.1,periteration = 8,angle_tol = 1.0e-1,Print=True,quick=False,**kwargs):

    # MEP Inputs

    # Arguments
    # - Energy is the energy functional
    # - mesh describes the finite element mesh
    # - element is the finite element used
    # - V is the function space
    # - bcs are the boundary conditions of the admissible class of functions
    # - coeff is dictionary running from 0 to k-1, containing the k coefficients
    #   of the energy functional

    # Keywords
    # - n is the number of images along the MEP
    # - cache_min would hold the minima of the system
    # - cache_saddle would hold the saddle points of the system
    # - para describes whether 'linear' or 'cubic' interpolation is used between images

    # - initial describes the initialisation of the MEP.'stright' is a straight
    #   line between the two minima. 'saddle' is a piecewise linear line between
    #   the two minima via a saddle point.

    # - If initial = 'saddle', then order controls the order that the saddle points
    #   are tested. E.g. if saddle points are indexed 0 and 1. order = [1,0] would
    #   first test saddle 1 then saddle 0. If order = None then the order is done
    #   by the order of the height of the saddle points, lowest to highest.

    # - start and end are lists control the minima an MEP is calculated between and the
    #   order in which they are calculatd. If they are equal to None, then all the minima
    #   are cycled through

    # - If pvdfile = True then the MEP is saved as a pvdfile every 'pvdskip' Iterations
    # - If intermediategraphs = True, an Energy against arc length graph is plotted
    #   ever iteration
    # - If transitionplots = True, if the problem is 1D then the MEP is plotted
    #   every 'interskip' iterations.
    # - dtset is the time step used
    # - periteration is the number of time steps done before re-parametrisation
    # - angle_tol is the angle tolerance of the MEP that is accepted
    # - Print is whether the code prints out the various stages of the code
    # - quick uses saddle point shortcuts to find the smallest maximum height of
    #   the MEP. This SHOULD NOT be used if one wishes to find the correct full MEP.

    # If the Print option is false then the output is surpressed. Useful when using BOBYQA
    if Print == False:
        blockPrint()

    # Find dimension of system
    dimension = V.dim()/len(mesh.coordinates())
    # Raise error if using cubic spline with vector-valued function
    if dimension > 1 and para == 'cubic':
        raise NameError('Cubic spline not yet implemented for vector-valued functions')


    # Used as starting guess for later solves
    filler = 1.0 if dimension ==1 else [1.0]*dimension
    # Initialise functions
    phi = Function(V)    # Will be used mostly for finding new updated image along path
    udummy = Function(V)  # Mostly used as a dummy variable

    phi_prevs = {}       # A dictionary containing past 4 iterates of an image
    for i in xrange(4):
        phi_prevs[i] = Function(V)


    phi_dict ={} # A dictionary containing the images
    phiA ={}     # Will contain the tangent of the path and sometimes used as a dummy variable
    rv = {}      # Will contain the Riesz representation of J' and sometimes used as a dummy variable
    for i in xrange(n):
        phi_dict[i] = Function(V)
        phiA[i] = Function(V)
        rv[i] = Function(V)


    pack_phi = {} # Will eventually hold MEPs

    v = TestFunction(V) # Test function

    toler3=[]
    diff_toler3 =[]
    # Set up zero boundary conditions for calculating Riesz representation of Frechet derivatives
    bcs_0 = DirichletBC(V, 0.0, "on_boundary") if dimension == 1 \
        else DirichletBC(V, [0.0]*dimension, "on_boundary")
    # Find the minima, saddle points and maxima of the problem if not already given
    if cache_min == None or cache_saddle == None:
        [cache_min, cache_saddle] = findsteadystates(Energy,rhs,coeff,mesh,phi,v,V,bcs\
                                                    ,transitionplots)

    # If there is only one saddle point, return true
    def only1(l):
        return l.count(True) == 1

    # Calculate heights of the saddle points
    height = []

    for i in xrange(len(cache_saddle.keys())):
        height.append(assemble(Energy(cache_saddle[i],coeff)))
    print "Saddle Point Heights: %s" %height


    # Calculate heights of minima
    height_min = []
    for i in xrange(len(cache_min.keys())):
        height_min.append(assemble(Energy(cache_min[i],coeff)))
    print "Minima Heights: %s" %height_min



    # If the option 'order' is None, then calculate order based on heights.
    # from smallest to largest
    if order == None:
        order = [b[0] for b in sorted(enumerate(height),key=lambda i:i[1])]
    if len(order) == 0:
        order = [0]
        height.append(2e-3)

    print 'order = %s' %order
    order.append(len(height))
    one_saddle = False

    # Set time step
    dt2 = dtset
    dt = Constant(dt2)


    # Initialise matrix to hold Freidlin-Wentzell action values
    S = np.zeros((len(cache_min.keys()),len(cache_min.keys())))

    # Initialise coefficients used in cubic spline
    four = 4.0*np.ones((n,1))
    ones = np.ones((n-1,1))
    A = np.diag(four[:,0],0) + np.diag(ones[:,0],-1) + np.diag(ones[:,0],1)
    A[0,0] = 2.0
    A[-1,-1] = 2.0
    B = sc.linalg.inv(A)

    # Initialise uniform arc length coefficients (alpha)
    a0 = np.linspace(0.0,1.0,n)

################################# Plot initial distribution
    # If the order of minima in which we calculate the MEP between is not given
    # then do in chronological order
    if start == None:
        start = range(len(cache_min))
    if end == None:
        end = range(len(cache_min))
    for q in start:
        for w in end:

          # Initialise with all saddles as feasible saddle points
          possible_saddles = [True]*(len(height)+1)
          possible_saddles[-1] = False
          # Do not repeat finding the MEP between two minima
          if w == q: continue
          if w<q: continue

          for saddle in order:

                # When using quick = True and only highest point of MEP is needed
                # then if there is only one feasible saddle point, highest point of
                # MEP can be found immediately

                if one_saddle == True:
                    one_saddle = False
                    S[q][w] = 2.0*(height[one_saddle_iter] - assemble(Energy(cache_min[q],coeff)))
                    S[w][q] = 2.0*(height[one_saddle_iter] - assemble(Energy(cache_min[w],coeff)))
                    break

                # If saddle is found to be infeasible, then skip the calculation of the MEP
                # via this saddle
                if possible_saddles[saddle] == False:
                   continue

                # Reset time steps in case they are altered
                dt2 = dtset
                dt.assign(Constant(dt2))

                # image_iter is used to count the current image we wish to evolve
                image_iter=0

                # Initialisation if we have an initial guess via a saddle
                if initial == 'saddle':
                    udummy.assign(cache_saddle[saddle])
                    s =[0.0]
                    s.append(s[0] + errornorm(cache_min[q],udummy,'H1'))
                    s.append(s[1] + errornorm(cache_min[w],udummy,'H1'))
                    s = np.asarray(s)
                    ainitial = s/s[-1]
                    piece = [cache_min[q],udummy,cache_min[w]]

                # If we want a .pvd file output
                if pvdfile == True:
                    pvd = File("MEP.pvd")


                # iter counts the number of iterations
                iter = 0

                # Initialise vector to hold convergence
                toler = np.ones(n); toler[0] = 0.0; toler[n-1] = 0.0
                diff_toler = np.zeros(n)
                pasttoler = 1.0


                # Here we evolve the images, while max_{0<=i<=n-1} sin(theta_i) > tol
                while max(abs(toler)) > angle_tol:

                    # If we only care for the highest value of the MEP, then test if there is
                    # only one saddle point
                    if quick == True:
                        phi_dict[0].assign(cache_min[q])
                        phi_dict[n-1].assign(cache_min[w])
                        one_saddle = only1(possible_saddles)

                    one_saddle_iter = saddle

                    # If only one saddle, then break
                    if one_saddle == True:
                        break
                    image_iter = 0

                    # Iterate through images
                    for a in a0:
                        # If this is the first iteration, set up initial guess
                        if iter == 0:
                            if initial=='straight':
                                # Straight line between the two minima
                                phi.assign(Constant(1-a)*cache_min[q] + Constant(a)*cache_min[w])
                            if initial == 'saddle':
                                # Piecewise linear line via relevant saddle point
                                udummy.assign(cache_saddle[saddle])
                                phi.assign(linspline(a,ainitial,piece))

                            phi_prevs[0].assign(phi)

                        else:
                            # If not first iteration, then assign relevant image to phi_prevs
                            phi_prevs[0].assign(phi_dict[image_iter])

                        # Time stepping scheme
                        for ntimestep in xrange(periteration):
                            # Keep end point images fixed to minima
                            if a == 0.0:
                                phi.assign(phi_prevs[0])
                            elif a == 1.0:
                                phi.assign(phi_prevs[0])

                            # As more time steps used, we employ higher order numerical schemes
                            elif ntimestep == 0: # Crank-Nicolson
                                phi.assign(phi_prevs[0])
                                phi.assign(TimeSolver(Energy,coeff,mesh,"CN"\
                                            ,phi,phi_prevs,v,dt,bcs))
                            elif ntimestep == 1: # BDF-2
                                phi.assign(TimeSolver(Energy,coeff,mesh,"bdf2"\
                                            ,phi,phi_prevs,v,dt,bcs))
                            elif ntimestep == 2: # BDF-3
                                phi.assign(TimeSolver(Energy,coeff,mesh,"bdf3"\
                                            ,phi,phi_prevs,v,dt,bcs))
                            else:                # BDF-4
                                phi.assign(TimeSolver(Energy,coeff,mesh,"bdf4"\
                                            ,phi,phi_prevs,v,dt,bcs))

                            phi_prevs[3].assign(phi_prevs[2])
                            phi_prevs[2].assign(phi_prevs[1])
                            phi_prevs[1].assign(phi_prevs[0])
                            phi_prevs[0].assign(phi)


                        phiA[image_iter].assign(phi) # phiA is used to hold new iterate of image

                        # Increment image number
                        image_iter += 1

                    # Here we find the non-uniform arc length of path
                    s =[0.0]
                    for i in xrange(1,n):
                        s.append(s[i-1] + errornorm(phiA[i],phiA[i-1],'H1'))
                    s = np.asarray(s)

                    a1 = s/s[i]


                    # Here we interpolate to find position of images with uniform arc-length
                    if para=='cubic':  # Using a cubic spline
                        for i in xrange(n):
                            rv[i].assign(cubspline(a0[i],a1,phiA,B))
                    elif para=='linear': # Using a linear spline
                        for i in xrange(n):
                            rv[i].assign(linspline(a0[i],a1,phiA))
                    else:
                        raise NameError('Pick a parameterisation of the line')

                    # Increment iteration number
                    iter += 1
                    if iter == 1:
                        pvdskip1 = pvdskip

                    # Assign new images to dictionary
                    for i in xrange(n):
                        phi_dict[i].assign(rv[i])
                        if pvdfile == True and iter == pvdskip1:
                            # Save .pvd file
                            udummy.assign(rv[i])
                            pvd << udummy

                    if iter == pvdskip1:
                            pvdskip1 += pvdskip

                    # Find energy value of images
                    energy_values = []
                    for i in xrange(n):
                        val = assemble( Energy(phi_dict[i],coeff) )
                        if i == 0:
                            maxval = val
                        if maxval < val:
                            maxval = val
                        energy_values.append(val)

                    # Find tangent along path of images
                    phiA[0].assign((linspline(1e-5,a0,rv)-rv[0])/1e-5)
                    phiA[n-1].assign((rv[n-1]-(linspline(1.0-1e-5,a0,rv)))/1e-5)

                    for i in xrange(1,n-1):
                        # Use central difference with linear or cubic spline
                        phiA[i].assign((linspline(a0[i]+1e-5,a0,rv)-linspline(a0[i]-1e-5,a0,rv))/2e-5)
                        #phiA[i].assign((cubspline(a0[i]+1e-5,a0,rv,B)-cubspline(a0[i]-1e-5,a0,rv,B))/2e-5)

                    for i in xrange(0,n):
                        # Find the Riesz representation of the Frechet derivative of the image
                        rv[i].assign(Constant(filler))
                        R = innerh1(rv[i],v) + rhs(Energy,phi_dict[i],v,coeff,mesh)
                        solve(R==0,rv[i],bcs_0)

                        # Find angle between the tangent of the path and the gradient of the
                        # energy landscape
                        toler[i] = np.sin(angleh1(rv[i],phiA[i]))
                        diff_toler[i] = residue(rv[i],phiA[i])

                    # Set angles at the end to zero
                    toler[0] = 0.0
                    toler[n-1] = 0.0
                    # Find maximum sin(angle) error
                    diff_toler2 = max(abs(diff_toler))
                    diff_toler3.append(diff_toler2)
                    toler2 = max(abs(toler))
                    toler3.append(toler2)
                    # Print interation number and relevant information
                    print "Iteration: %s - dt = %s - Per Iteration: %s - Sin(Angle): %s - Residue: %s \
                            - Height: %s" %(iter,dt2,periteration,toler2, diff_toler2,maxval)
                    # Break after 1000 iterations
                    if iter >=  1000:
                        print "Iterations: %s" %(iter)
                        print "Tolerance Reached: %s" %toler2
                        break

                    # If convergence occurs, saddle point found, break MEP search
                    if toler2 < angle_tol:
                        possible_saddles = [False]*len(possible_saddles)


                    # If the image with the highest energy value is 1/10th less
                    # than the height of the saddle point then the saddle point
                    # is deemed infeasible and a new saddle point is used
                    if initial == 'saddle':
                        if maxval < height[saddle] - height[saddle]/10.0:
                            print "Saddle %s is the incorrect saddle" %saddle
                            break

                    for i in xrange(len(height)):
                        if i == saddle:
                            continue
                        elif maxval < height[i] - 0.5:
                            possible_saddles[i] = False

                    # Plot energy profile of images
                    if intermediategraphs == True:
                        xx = np.linspace(0,1,200)
                        f = np.zeros_like(xx)
                        f2 = np.zeros_like(a0)
                        for i in np.arange(0, xx.size):
                                udummy.assign(cubspline(xx[i],a0,phi_dict,B))
                                f[i] = assemble(Energy(udummy))
                        for i in np.arange(0, a0.size):
                                f2[i] = assemble(Energy(phi_dict[i]))
                        plt.plot(xx,f,'b',a0,f2,'ro',linewidth=3)
                        plt.ylabel('Energy Potential')
                        plt.xlabel('H1 Norm')
                        plt.title('Energy Diagram (%s) -> (%s)' %(q,w))
                        plt.savefig('energy_profile_%s.png' %(iter))
                        plt.hold(False)

                    # If system only 1D can plot .png images of path
                    if transitionplots == True and iter == 1:
        					interskip1 = interskip
                    if transitionplots == True and iter == interskip1:
                     #   ax1 = plt.subplots(a)
                        xx = np.linspace(0,1,1000)
                        f = np.zeros((np.size(xx),dimension))
                        for i2 in xrange(n):
                            for i in np.arange(0, xx.size):
                                f[i,:] = phi_dict[i2](xx[i])
                            for j in xrange(dimension):
                                plt.plot(xx,f[:,j],linewidth=3,label='Dimension %s' %(j+1))
                            plt.ylim([-3.5,3.5])
                            plt.title('Image: %s Iteration %s' %(i2,iter))
                            #plt.legend(loc='upper left',fontsize=15)
                            plt.savefig('iter_%s_image_%s.png' %(iter,i2), bbox_inches='tight')
                            plt.close("all")
                       # for i in xrange(n):
                    if transitionplots == True and iter == interskip1:
        					interskip1 += interskip

                # Once MEP found, save number in matrix holding Freidlin-Wentzell actions
          if initial == 'saddle':
                S[q][w] = 2.0*(height[one_saddle_iter] - assemble(Energy(cache_min[q],coeff)))
                S[w][q] = 2.0*(height[one_saddle_iter] - assemble(Energy(cache_min[w],coeff)))
          elif initial == 'straight':
                S[q][w] = 2.0*(maxval - assemble(Energy(cache_min[q],coeff)))
                S[w][q] = 2.0*(maxval - assemble(Energy(cache_min[w],coeff)))

          for i in xrange(0,n): # Store MEP
                pack_phi["%s%s_%s" %(q,w,i)] = Function(V)
                pack_phi["%s%s_%s" %(q,w,i)].assign(phi_dict[i])

    enablePrint()
    #print toler3
    #print diff_toler3
    return [S,pack_phi,cache_saddle[one_saddle_iter],phiA,rv]
