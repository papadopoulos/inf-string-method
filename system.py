# -*- coding: utf-8 -*-
"""
Interface code to find the MEP of the system of pdes example

Author:
"""
from inf_string_algorithms import *

set_log_active(False) # Suppress FEniCS output

# Use a linear spline, initial straight line guess, 60 images, time step of 0.1, 8 time
# steps per iteration, angle tolerance of 0.09 and plot the MEPs every 2 iterations
options = dict(para = 'linear',initial = 'straight'\
    ,n = 60,dtset = 0.1,periteration = 8,angle_tol = 9.0e-2\
    , transitionplots = True,interskip = 2)


# Define mesh, finite element, function space and boundary conditions
mesh = IntervalMesh(3000,0,1)
element = VectorElement("Lagrange", mesh.ufl_cell(),1,dim=2)
V = FunctionSpace(mesh,element)
bcs = DirichletBC(V, (0.0,0.0), "on_boundary")

# Define energy functional
def Energy(u,coeff):
     (u0,u1) = split(u)
     return 0.5*inner(grad(u),grad(u))*dx + coeff[0]*(1-u0**2-u1**2)**2*dx \
                - u0**2*u1*dx

# Define parameters
coeff = [Constant(10.0)]
# Calculate MEP, retrieving Freidlin-Wentzell action and images
# phi is a dictionary in the form phi["ij_k"]. i and j are the index of the minima
# and k is the image along the string running from 0 to n-1.
[S,phi,_,_,_] =  MEP(Energy,mesh,element,V,bcs,coeff,**options)
