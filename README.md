# README #

### What is this repository for? ###

This repository contains the code for finding minimum energy paths in function spaces using the infinite
dimensional string method as detailed in Ioannis Papadopoulos' thesis submitted for the
degree of M.Sc. in Mathematical Modelling and Scientific Computing at the University of Oxford in Trinity
Term 2017.

### How do I get set up? ###

The code is written in Python using FEniCS: a finite element solver platform. This can be run inside docker
images. Instructions for set up can be found at http://fenicsproject.org/download.

The core algorithms for finding the minimum energy path are found in inf_string_algorthms.py.


### Who do I talk to? ###

Ioannis Papadopous (papadopoulos@maths.ox.ac.uk)

### Interface Code ###

#### Infinite-Dimensional Energy Functional####

euler_elastica.py - Finds the MEP of the Euler elastica where lambda = 4.3, mu = 0.5 and outputs the Freidlin-Wentzell action. The MEP is also saved as .png files.

allen_cahn.py - Finds the MEP of the 2D Allen Cahn energy functional where delta = 0.1. The MEP is also saved as .pvd files.

system.py - Finds the MEP of the system of pdes example in Section 2.2.9.

#### Optimisation of the Freidlin-Wentzell action ####

euler_elastica_optimisation.py - Uses the calculated explicit derivative of the energy functional (see thesis) to optimise Freidlin-Wentzell action with two parameters.

modified_euler_elastica_optimisation.py - Uses the calculated explicit derivative of the energy functional (see thesis) to optimise Freidlin-Wentzell action with two parameters, one infinite dimensional.

### Disclaimer ###

This repository is for the sole use of the assessment of Ioannis Papadopoulos' thesis. No guarantees are made
by the author and the author is not liable for any damages, commercial or otherwise.
