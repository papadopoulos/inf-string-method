# -*- coding: utf-8 -*-
"""
Interface code to find the MEP of the Euler elastica potential

Author: Ioannis Papadopoulos
"""
from inf_string_algorithms import * # Author's backend algorithms
import sys, os

# Suppress FEniCS output
set_log_active(False)

# cubic spline, initial guess via saddle, 20 images, time step of 0.1, 8 time
# steps per iteration, angle tolerance of 0.1, plot MEPs every 2 iterations
options = dict(para = 'cubic',initial = 'saddle'\
    ,n = 20,dtset = 0.1,periteration = 8,angle_tol = 1e-1\
    ,transitionplots = True,interskip = 2)

# Define mesh, finite element, function space and boundary conditions
mesh = IntervalMesh(3000,0,1)
element = FiniteElement("Lagrange", mesh.ufl_cell(),1)
V = FunctionSpace(mesh,element)
bcs = DirichletBC(V, 0.0, "on_boundary")

# Define energy functional
def Energy(u,coeff):
     return 0.5*inner(grad(u),grad(u))*dx + coeff[0]**2*cos(u)*dx + coeff[1]*u*dx

# Define parameters
coeff = [Constant(4.0),Constant(0.5)]
# Calculate MEP, retrieving Freidlin-Wentzell action and images
# phi is a dictionary in the form phi["ij_k"]. i and j are the index of the minima
# and k is the image along the string running from 0 to n-1.
[S,phi,_,_,_] =  MEP(Energy,mesh,element,V,bcs,coeff,**options)
print S # Print Freidlin-Wentzell action matrix
