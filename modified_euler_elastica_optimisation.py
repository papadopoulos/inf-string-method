# -*- coding: utf-8 -*-
"""
Interface code to find for the optimisation of the modified Euler elastica
Freidlin-Wentzell action using steepest descent coupled with explicit
derivatives. Here the parameter is a function living in L^2

Author: Ioannis Papadopoulos
"""
from inf_string_algorithms import *
import time

# Suppress FEniCS output
set_log_active(False)

# cubic spline, initial guess via saddle, 20 images, time step of 0.1, 8 time
# steps per iteration, angle tolerance of 0.1, suppress MEP algorithm output

# quick = True makes some shortcuts to speed up the code. We do not advise the use
# of this option except in the examples given in this repository
options = dict(para = 'cubic',initial = 'saddle'\
    ,n = 20,dtset = 0.1,periteration = 8,angle_tol = 1.0e-1,Print=False,quick=True)

# Define mesh, finite element, function space and boundary conditions
mesh = IntervalMesh(3000,0,1)
element = FiniteElement("Lagrange", mesh.ufl_cell(),1)
V = FunctionSpace(mesh,element)
bcs = DirichletBC(V, 0.0, "on_boundary")

# Define Euler elastica energy functional
def Energy(u,coeff):
     return 0.5*inner(grad(u),grad(u))*dx + coeff[0]**2*cos(u)*dx + coeff[1]*u*dx

# Define desired values of Freidlin-Wentzell action
goal1 = 10.66271522 # S_12(4.3,0.5)
goal2 = 15.0

def objective_functional(S): # Define objective functional
     return (S[0,1]-goal1)**2 + (S[1,0]-goal2)**2

def evaluate(coeff): # Find MEP, Freidlin-Wentzell matrix, MEP and saddle point
    n = options['n']
    [S,phi,phi_as,_,_] =  MEP(Energy,mesh,element,V,bcs,coeff,**options)
    return [objective_functional(S), S,phi_as, phi["01_0"], phi["01_%s"%(n-1)]]

# Hard code explicit derivative of energy functional with respect to lambda and mu
def lmbda_deriv(u,coeff):
    L = inner(Constant(2.0)*coeff[0]*cos(u),v)*dx - inner(deriv,v)*dx
    solve(L==0,deriv)
    return deriv
def mu_deriv(u,coeff):
    return assemble(u*dx)

# Find descent direction for lambda
def direction1():
    deriv1.assign(lmbda_deriv(phi_as,coeff))
    deriv2.assign(lmbda_deriv(phi_start,coeff))
    deriv3.assign(lmbda_deriv(phi_end,coeff))
    return 2.0 * ((S[0,1]-goal1))*(2.0*(deriv1-deriv2))\
        + 2.0 * ((S[1,0]-goal2))*(2.0*(deriv1-deriv3))
# Find descent direction for mu
def direction2():
    return 2.0 * ((S[0,1]-goal1))*(2.0*(mu_deriv(phi_as,coeff)-mu_deriv(phi_start,coeff)))\
        + 2.0 * ((S[1,0]-goal2))*(2.0*(mu_deriv(phi_as,coeff)-mu_deriv(phi_end,coeff)))


# Initialise lambda, dummy functions and test function
lmbda = Function(V)
deriv = Function(V)
deriv1 = Function(V)
deriv2 = Function(V)
deriv3 = Function(V)
deriv.assign(Constant(1.0))
v = TestFunction(V)

lmbda.interpolate(Constant(4.3)) # Initial guess for lambda and mu
coeff = [lmbda, 0.5]

transitionplots = True # For plotting lambda in each iteration

# Evaluate initial functional_val, Freidlin-Wentzell matrix, minima and saddle point
[functional_val, S,phi_as, phi_start, phi_end] = evaluate(coeff)
[functional_val, S,phi_as, phi_start, phi_end] = evaluate(coeff)
iteration =1
start = time.time()
while functional_val > 1e-20: # While objective function greater than 1e-20

    d1 = direction1() # Find descent direction of lambda
    d2 = direction2() # Find descent direction of mu
    step =  1e-3  # Step size of steepest descent

    # Increment parameters
    coeff[0].assign(coeff[0]-step*d1)
    coeff[1] = coeff[1] - step*d2

    [functional_val, S,phi_as, phi_start, phi_end] = evaluate(coeff)
    print 'Iteration: %s, Obj Function = %s, mu: %s, Step size: %s, Time: %s' %(iteration, functional_val, coeff[1], step,time.time()-start)


    # Plot lambda
    xx = np.linspace(0,1,1000)
    f = []
    if transitionplots == True:
        xx = np.linspace(0,1,1000)
        f = np.zeros((np.size(xx),1))
        for i in np.arange(0, xx.size):
            f[i]= lmbda(xx[i])
        plt.plot(xx,f,linewidth=3)
        plt.ylabel('$\lambda$(x)',fontsize=25)
        plt.xlabel('x',fontsize=25)
        plt.ylim([4.2,4.7])
        plt.title('$\lambda$ (iteration: %s)' %iteration)
        plt.savefig('lmbda_iteration_%s' %iteration, bbox_inches='tight')
        plt.close("all")
    iteration +=1
