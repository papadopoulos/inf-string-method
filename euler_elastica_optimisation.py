# -*- coding: utf-8 -*-
"""
Interface code to find for the optimisation of the Euler elastica
Freidlin-Wentzell action using steepest descent coupled with explicit
derivatives.

Author: Ioannis Papadopoulos
"""
from inf_string_algorithms import * # Author's backend algorithms
import time

# Suppress FEniCS output
set_log_active(False)

# cubic spline, initial guess via saddle, 20 images, time step of 0.1, 8 time
# steps per iteration, angle tolerance of 0.1, suppress MEP algorithm output

# quick = True makes some shortcuts to speed up the code. We do not advise the use
# of this option except in the examples given in this repository
options = dict(para = 'cubic',initial = 'saddle'\
    ,n = 20,dtset = 0.1,periteration = 8,angle_tol = 1.0e-1,Print=False,quck=True)

# Define mesh, finite element, function space and boundary conditions
mesh = IntervalMesh(3000,0,1)
element = FiniteElement("Lagrange", mesh.ufl_cell(),1)
V = FunctionSpace(mesh,element)
bcs = DirichletBC(V, 0.0, "on_boundary")

# Define Euler elastica energy functional
def Energy(u,coeff):
     return 0.5*inner(grad(u),grad(u))*dx + coeff[0]**2*cos(u)*dx + coeff[1]*u*dx

# Define desired values of Freidlin-Wentzell action
goal1 = 10.66271522 # S_12(4.3,0.5)
goal2 = 15.0


def objective_functional(S): # Define objective functional
     return (S[0,1]-goal1)**2 + (S[1,0]-goal2)**2

def evaluate(coeff): # Find MEP, Freidlin-Wentzell matrix, MEP and saddle point
    n = options['n']
    [S,phi,phi_as,_,_] =  MEP(Energy,mesh,element,V,bcs,coeff,**options)
    return [objective_functional(S), S,phi_as, phi["01_0"], phi["01_%s"%(n-1)]]

# Hard code explicit derivative of energy functional with respect to lambda and mu
def lmbda_deriv(u,coeff):
    return assemble(cos(u)*dx)
def mu_deriv(u,coeff):
    return assemble(u*dx)

# Find descent direction for lambda
def direction1():
    return 2.0 * ((S[0,1]-goal1))*(2.0*(lmbda_deriv(phi_as,coeff)-lmbda_deriv(phi_start,coeff)))\
        + 2.0 * ((S[1,0]-goal2))*(2.0*(lmbda_deriv(phi_as,coeff)-lmbda_deriv(phi_end,coeff)))
# Find descent direction for mu
def direction2():
    return 2.0 * ((S[0,1]-goal1))*(2.0*(mu_deriv(phi_as,coeff)-mu_deriv(phi_start,coeff)))\
        + 2.0 * ((S[1,0]-goal2))*(2.0*(mu_deriv(phi_as,coeff)-mu_deriv(phi_end,coeff)))


# Initial parameter guess
coeff = np.array([4.3,0.5])

# Evaluate initial functional_val, Freidlin-Wentzell matrix, minima and saddle point
[functional_val, S,phi_as, phi_start, phi_end] = evaluate(coeff)
iteration =1
start = time.time()
while functional_val > 1e-20: # While objective function greater than 1e-20
    d1 = direction1() # Find descent direction of lambda
    d2 = direction2() # Find descent direction of mu
    direction = np.array([d1,d2])
    step =  1e-3 # Step size of steepest descent
    coeff = coeff -  step*direction # Increment parameters
    [functional_val, S,phi_as, phi_start, phi_end] = evaluate(coeff) # Find new objective functional values and MEP
    print 'Iteration: %s, Obj Function = %s, param: %s, Step size: %s, Time: %s' %(iteration, functional_val, coeff, step,time.time()-start)
    iteration +=1
