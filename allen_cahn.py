# -*- coding: utf-8 -*-
"""
Interface code to find the MEP of the 2D Allen-Cahn potential

Author: Ioannis Papadopoulos
"""
from inf_string_algorithms import * # Author's backend algorithms

# Suppress FEniCS output
set_log_active(False)

# cubic spline, initial guess via saddle, 20 images, time step of 0.1, 8 time
# steps per iteration, angle tolerance of 0.1, plot MEP as .pvd file every 5 iterations
options = dict(para = 'cubic',initial = 'saddle'\
    ,n = 20,dtset = 0.1,periteration = 8,angle_tol = 3e-1\
    ,pvdfile = True,pvdskip = 5)

# Define square mesh, finite element, function space and boundary conditions
mesh = UnitSquareMesh(50,50)
element = FiniteElement("Lagrange", mesh.ufl_cell(),1)
V = FunctionSpace(mesh,element)
bc1 = DirichletBC(V, 1.0, "on_boundary && (near(x[0], 0.0) || near(x[0], 1.0))")
bc2 = DirichletBC(V, -1.0, "on_boundary && (near(x[1], 0.0) || near(x[1], 1.0))")
bcs = [bc1, bc2]

# Define energy functional

def Energy(u,coeff):
     return (0.5*inner(grad(u), grad(u))*coeff[0] \
                +  1./coeff[0]*0.25*(1.0 - u**2)**2) * dx
# Define parameters
coeff = [Constant(0.1)]
# Calculate MEP, retrieving Freidlin-Wentzell action and images
# phi is a dictionary in the form phi["ij_k"]. i and j are the index of the minima
# and k is the image along the string running from 0 to n-1.
[S,phi,_,_,_] =  MEP(Energy,mesh,element,V,bcs,coeff,**options)
